<?php

namespace Functional\ContactBundle\Controller;

/**
 * @group contact
 */
class ContactControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function listing_page(\FunctionalTester $i)
    {
        $i->amOnPage('/contact');
        $i->see('Name');
        $i->see('Contact');
        $i->see('Subject');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function edit_page(\FunctionalTester $i)
    {
        $i->amOnPage('/contact');
        $i->see('Edit');
        $i->click('Edit');
        $i->see('Edit Contact');
        $i->see('Email');
        $i->see('Subject');
        $i->see('Message');
        $i->canSeeResponseCodeIs(200);
    }
}
