<?php

namespace Functional\ContactBundle\Controller;

use Codeception\Example;

/**
 * @group delete
 */
class DeleteRequestCest
{
    /**
     * @param \FunctionalTester $i
     *
     * @dataProvider deleteUrlsProvider
     */
    public function delete_change(\FunctionalTester $i, Example $urls)
    {
        $i->sendAjaxPostRequest($urls['url'], [
            'id' => 1,
        ]);
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @return array
     */
    public function deleteUrlsProvider()
    {
        return [
            ['url' => '/contact/delete'],
        ];
    }
}
