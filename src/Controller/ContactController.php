<?php

namespace Kisphp\ContactBundle\Controller;

use Kisphp\ContactBundle\Form\ContactForm;
use Kisphp\FrameworkAdminBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    const SECTION_TITLE = 'section.title.contact';
    const MODEL_NAME = 'model.contact';
    const ENTITY_FORM_CLASS = ContactForm::class;
    const LISTING_TEMPLATE = '@Contact/Contact/index.html.twig';

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'adm_contact_edit',
        self::LIST_PATH => 'adm_contact',
        self::ADD_PATH => '',
        self::STATUS_PATH => 'adm_contact_status',
        self::DELETE_PATH => 'adm_contact_delete',
    ];
}
