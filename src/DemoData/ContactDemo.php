<?php

namespace Kisphp\ContactBundle\DemoData;

use Kisphp\FrameworkAdminBundle\Fixtures\AbstractDemoData;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ContactDemo extends AbstractDemoData
{
    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    public function loadDemoData(InputInterface $input, OutputInterface $output)
    {
        $this->createContact($output);
    }

    /**
     * @param OutputInterface $output
     */
    protected function createContact(OutputInterface $output)
    {
        $model = $this->getContainer()->get('model.contact');

        $entity = $model->createEntity();

        $entity->setName('John Doe');
        $entity->setPhone('1111111111111');
        $entity->setEmail('test@example.com');
        $entity->setSubject('my subject');
        $entity->setMessage('my message');

        $model->persist($entity);

        $model->flush();

        $output->writeln('<info>Created contact request</info>');
    }
}
