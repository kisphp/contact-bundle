<?php

namespace Kisphp\ContactBundle\Model;

use Kisphp\ContactBundle\Entity\Contact;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\FrameworkAdminBundle\Model\AbstractModel;

class ContactModel extends AbstractModel
{
    const REPOSITORY = 'ContactBundle:Contact';

    /**
     * @return Contact|KisphpEntityInterface
     */
    public function createEntity()
    {
        return new Contact();
    }
}
