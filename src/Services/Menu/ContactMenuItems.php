<?php

namespace Kisphp\ContactBundle\Services\Menu;

use Kisphp\FrameworkAdminBundle\Services\MenuItemInterface;

class ContactMenuItems implements MenuItemInterface
{
    /**
     * @param \ArrayIterator $iterator
     */
    public function getMenuItems(\ArrayIterator $iterator)
    {
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'contact',
            'match_route' => 'contact',
            'path' => 'adm_contact',
            'icon' => 'fa-envelope',
            'label' => 'main_navigation.contact',
        ]);
    }
}
